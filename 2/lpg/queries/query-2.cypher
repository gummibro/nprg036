//Peter has something else from the same company that published Doom in his Library, what is it and what is its rating?

MATCH ({name: "Doom"})-[:Publisher]->({name: "Activision"})<-[:Publisher]-(m:Software)<-[:Contains]-(:Library) 
return m.name, m.rating