 What
    Choose a domain/topic for your homework. Create a conceptual model of that domain using UML Class Diagrams and a textual description of what is represented in the domain model. The consecutive homework parts will then be governed by this conceptual model. Quantitative requirements:

       1. 5 to 10 classes
       2. Each class 3 to 5 attributes, some with multiplicity 0..*
       3. At least 4 associations. Include association end multiplicities 0..*, 0..1, 1..1

How
    Use a tool that allows you to draw UML Class Diagrams and export them as SVG. For example, diagrams.net. The homework is to be turned in via the study system by a group leader.

        1. A zipped file named NPRG036-<groupID>.zip, e.g. NPRG036-T1G4.zip
        2. Folder 1 containing domain.svg with the image of the conceptual model and domain.txt in UTF-8 containing the textual description of the domain model.
